import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import GridRow from './GridRow';
import Square from './Square';

export default class Grid extends Component {
    constructor(props) {
        super();
        this.state = { numberOfSquares: props.numOfSquares };
        this.numberOfSquares = props.numOfSquares;
        this.rows = [];
        for (var i = 0; i < this.numberOfSquares; i++) {
            this.rows.push({ row: < GridRow key={i} gridRowId={i} numberOfSquares={this.numberOfSquares} /> });
        }
    }

    SetNumber = (text) => {
        this.setState({ numberInputted: text && parseInt(text) ? parseInt(text) : 5 });
    }


    render() {
        return (<View style={GridStyles.Board}>{
            this.rows.map(x => x.row)
        }
        </View>
        )
    }

}
const GridStyles = StyleSheet.create({
    Board: {
        width: '85%',
        height: '80%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
});    