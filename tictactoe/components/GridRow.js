import React, { Component } from 'react';
import { View, TextInput } from 'react-native';
import Square from './Square';

export default class GridRow extends Component{
    constructor(props){
        super(props);
        this.numberOfSquares = props.numberOfSquares;
        this.cells = [];
        for(var i = 0; i< this.numberOfSquares; i++){
            this.cells.push({'cell' : <Square key={i + props.gridRowId} numberOfSquares={ this.numberOfSquares}/>});
        }
    }

    render(){
        return(
            <View style={{flexDirection:'row', height:'25%'}}>
                { 
                  //  this.cells[0].cell
                    this.cells.map(x => x.cell) 
                }
            </View>
        ) 
    } 
}  